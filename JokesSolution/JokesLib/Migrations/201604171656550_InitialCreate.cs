namespace JokesLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Administrators",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Jokes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Description = c.String(),
                        CreateDT = c.DateTime(nullable: false),
                        PublishDT = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        TotalVotes = c.Int(nullable: false),
                        RateValue = c.Int(nullable: false),
                        UserId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AuthUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AuthUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        FacebookId = c.String(),
                        GooglePlusId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Jokes", "UserId", "dbo.AuthUsers");
            DropIndex("dbo.Jokes", new[] { "UserId" });
            DropTable("dbo.AuthUsers");
            DropTable("dbo.Jokes");
            DropTable("dbo.Administrators");
        }
    }
}
