namespace JokesLib.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeduniqueidentifiertype : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserFavouriteJokes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        JokeId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        AddedToFavouritesDT = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Jokes", t => t.JokeId, cascadeDelete: true)
                .ForeignKey("dbo.AuthUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.JokeId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRatedJokes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        AuthUserId = c.Guid(nullable: false),
                        JokeId = c.Guid(nullable: false),
                        RateValue = c.Int(nullable: false),
                        RateDT = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Jokes", t => t.JokeId, cascadeDelete: true)
                .ForeignKey("dbo.AuthUsers", t => t.AuthUserId, cascadeDelete: true)
                .Index(t => t.AuthUserId)
                .Index(t => t.JokeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRatedJokes", "AuthUserId", "dbo.AuthUsers");
            DropForeignKey("dbo.UserRatedJokes", "JokeId", "dbo.Jokes");
            DropForeignKey("dbo.UserFavouriteJokes", "UserId", "dbo.AuthUsers");
            DropForeignKey("dbo.UserFavouriteJokes", "JokeId", "dbo.Jokes");
            DropIndex("dbo.UserRatedJokes", new[] { "JokeId" });
            DropIndex("dbo.UserRatedJokes", new[] { "AuthUserId" });
            DropIndex("dbo.UserFavouriteJokes", new[] { "UserId" });
            DropIndex("dbo.UserFavouriteJokes", new[] { "JokeId" });
            DropTable("dbo.UserRatedJokes");
            DropTable("dbo.UserFavouriteJokes");
        }
    }
}
