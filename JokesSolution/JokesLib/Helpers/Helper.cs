﻿using JokesLib.Implementations.SocialMedia;
using JokesLib.Interfaces.SocialMedia;
using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JokesLib.Helpers
{
    public class Helper
    {
        public static string EncryptMD5(string input)
        {
            if (String.IsNullOrEmpty(input)) { return String.Empty; }

            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(input));
            byte[] result = md5.Hash;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }
        public static string GetCurrentDomain()
        {
            return HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host +
                (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
        }
        public static string BuildQuery(Dictionary<string, string> queries)
        {
            if (queries == null || queries.Count == 0) { return String.Empty; }
            StringBuilder sb = new StringBuilder();
            sb.Append("?");
            foreach(var q in queries)
            {
                sb.AppendFormat("{0}={1}&", q.Key, q.Value);
            }
            return sb.ToString().TrimEnd('&');
        }
    }
}
