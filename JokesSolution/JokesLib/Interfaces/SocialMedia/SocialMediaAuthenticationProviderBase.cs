﻿using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Interfaces.SocialMedia
{
    public abstract class SocialMediaAuthenticationProviderBase
    {
        protected string _appId { get; set; }
        protected string _secretKey { get; set; }
        protected string _callbackUrl { get; set; }
        public SocialMediaAuthenticationProviderBase(string appId, string secretKey, string callbackUrl)
        {
            _appId = appId;
            _secretKey = secretKey;
            _callbackUrl = callbackUrl;
        }
        public abstract string GetRedirectUrl();
        public abstract AuthUser GetUserInfo();
    }
}
