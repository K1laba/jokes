﻿using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Interfaces.SocialMedia
{
    public interface ISocialMediaProviderHelper
    {
        SocialMediaAuthenticationProviderBase GetAuthenticationProvider(SocialMediaType mediaType);
        IAuthenticationRepository GetAuthenticationRepository(SocialMediaType mediaType);
    }
}
