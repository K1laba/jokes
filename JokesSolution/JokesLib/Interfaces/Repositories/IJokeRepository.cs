﻿using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Interfaces.Repositories
{
    public interface IJokeRepository
    {
        IEnumerable<Joke> LoadAll(Func<Joke, bool> predicate, out int totalItems);
        Joke Load(Guid id);
        void Add(Joke joke);
        void Edit(Joke joke);
        void Delete(Guid id);
        void Publish(Guid id);
        Joke Rate(Guid jokeId, int rateValue);
        void AddToFavourites(Guid jokeId, Guid userId);
        void DeleteFromFavourites(Guid jokeId, Guid userId);
        IEnumerable<Joke> LoadFavourites(Func<UserFavouriteJoke, bool> predicate, out int totalItems);
        IEnumerable<UserRatedJoke> LoadRated(Func<UserRatedJoke, bool> predicate, out int totalItems);
    }
}
