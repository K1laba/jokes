﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Interfaces.Repositories
{
    public interface IAdminLoginRepository
    {
        void Authenticate(string username, string password);
    }
}
