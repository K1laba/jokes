﻿using JokesLib.Database;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Implementations.Repositories.Mongo
{
    public class MongoGooglePlusAuthenticationRepository : IAuthenticationRepository
    {
        MongoDbContext _ctx = new MongoDbContext();
        public void Authenticate(Models.AuthUser userInfo)
        {
            if (userInfo == null) { return; }
            var filter = Builders<AuthUser>.Filter.Eq(u => u.GooglePlusId, userInfo.GooglePlusId);
            var retrieveQuery = _ctx.Users.Find<AuthUser>(filter).FirstOrDefaultAsync();
            retrieveQuery.Wait();
            if (retrieveQuery.Result != null)
            {
                userInfo.Id = retrieveQuery.Result.Id;
                userInfo.FavouriteJokes = retrieveQuery.Result.FavouriteJokes;
                userInfo.RatedJokes = retrieveQuery.Result.RatedJokes;
                var replaceQuery = _ctx.Users.ReplaceOneAsync<AuthUser>(i => i.GooglePlusId == userInfo.GooglePlusId,
                    userInfo, new UpdateOptions() { IsUpsert = true });
                replaceQuery.Wait();
            }
            else
            {
                userInfo.Id = Guid.NewGuid();
                _ctx.Users.InsertOneAsync(userInfo).Wait();
            }
            AuthUser.Instance = userInfo;
            AuthUser.Instance.IsAuthenticated = true;
        }
    }
}
