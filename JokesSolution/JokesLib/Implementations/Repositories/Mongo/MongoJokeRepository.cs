﻿using JokesLib.Database;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Implementations.Repositories.Mongo
{
    public class MongoJokeRepository : IJokeRepository
    {
        MongoDbContext _ctx = new MongoDbContext();
        public IEnumerable<Joke> LoadAll(Func<Joke, bool> predicate, out int totalItems)
        {
            var query = _ctx.Jokes.AsQueryable().Where(predicate);
            totalItems = query.Count();
            return query;
        }

        public Joke Load(Guid id)
        {
            var filter = Builders<Joke>.Filter.Eq(j => j.Id, id);
            var query = _ctx.Jokes.Find(filter).FirstOrDefaultAsync();
            query.Wait();
            return query.Result;
        }

        public void Add(Joke joke)
        {
            joke.Id = Guid.NewGuid();
            _ctx.Jokes.InsertOneAsync(joke).Wait();
        }

        public void Edit(Joke joke)
        {
            var filter = Builders<Joke>.Filter.Eq(j => j.Id, joke.Id);
            _ctx.Jokes.ReplaceOneAsync(filter, joke).Wait();
        }

        public void Delete(Guid id)
        {
            var filter = Builders<Joke>.Filter.Eq(j => j.Id, id);
            _ctx.Jokes.DeleteOneAsync(filter).Wait();
        }

        public void Publish(Guid id)
        {
            var filter = Builders<Joke>.Filter.Eq(j => j.Id, id);
            var update = Builders<Joke>.Update
                .Set(j => j.Status, JokeStatus.Published)
                .Set(j => j.PublishDT, DateTime.Now);
            _ctx.Jokes.UpdateOneAsync(filter, update).Wait();

        }

        public Joke Rate(Guid jokeId, int rateValue)
        {
            var filter = Builders<Joke>.Filter.Eq(j => j.Id, jokeId);
            var query = _ctx.Jokes.Find(filter).FirstOrDefaultAsync();
            query.Wait();
            if (query.Result == null) { return new Joke(); }
            var joke = query.Result;

            var userFilter = Builders<AuthUser>.Filter.Eq(u => u.Id, AuthUser.Instance.Id);
            var userQuery = _ctx.Users.Find(userFilter).FirstOrDefaultAsync();
            userQuery.Wait();
            var user = userQuery.Result;

            var currentRate = user.RatedJokes.FirstOrDefault(i => i.Joke != null && i.Joke.Id == jokeId);
            if (currentRate == null)
            {
                currentRate = new UserRatedJoke()
                {
                    Joke = joke,
                    RateDT = DateTime.Now,
                    RateValue = rateValue
                };
                user.RatedJokes.Add(currentRate);
                joke.TotalVotes++;

            }
            else
            {
                joke.RateValue -= currentRate.RateValue;
                currentRate.RateValue = rateValue;
                currentRate.RateDT = DateTime.Now;
            }
            joke.RateValue += rateValue;

            var jokeUpdateQuery = Builders<Joke>.Update
                                            .Set(j => j.RateValue, joke.RateValue)
                                            .Set(j => j.TotalVotes, joke.TotalVotes);
            var userUpdateQuery = Builders<AuthUser>.Update
                                            .Set(u => u.RatedJokes, user.RatedJokes);

            _ctx.Jokes.UpdateOneAsync(filter, jokeUpdateQuery).Wait();
            _ctx.Users.UpdateOneAsync(userFilter, userUpdateQuery).Wait();
            return joke;
        }
        public IEnumerable<UserRatedJoke> LoadRated(Func<UserRatedJoke, bool> predicate, out int totalItems)
        {
            var filter = Builders<AuthUser>.Filter.Eq(u => u.Id, AuthUser.Instance.Id);
            var query = _ctx.Users.Find(filter).FirstOrDefaultAsync();
            query.Wait();
            totalItems = query.Result.RatedJokes.Count;
            return query.Result.RatedJokes;
        }
        public void AddToFavourites(Guid jokeId, Guid userId)
        {
            var filter = Builders<Joke>.Filter.Eq(j => j.Id, jokeId);
            var jokeQuery = _ctx.Jokes.Find(filter).FirstOrDefaultAsync();

            var userFilter = Builders<AuthUser>.Filter.Eq(u => u.Id, userId);
            var userQuery = _ctx.Users.Find(userFilter).FirstOrDefaultAsync();
            jokeQuery.Wait();
            jokeQuery.Wait();

            var user = userQuery.Result;
            var joke = jokeQuery.Result;

            var alreadyAdded = user.FavouriteJokes.FirstOrDefault(j => j.Id == joke.Id) != null;
            if (user != null && !alreadyAdded) { user.FavouriteJokes.Add(joke); }

            var updateQuery = Builders<AuthUser>.Update.Set(u => u.FavouriteJokes, user.FavouriteJokes);
            _ctx.Users.UpdateOneAsync(userFilter, updateQuery).Wait();
        }

        public void DeleteFromFavourites(Guid jokeId, Guid userId)
        {
            var filter = Builders<Joke>.Filter.Eq(j => j.Id, jokeId);
            var jokeQuery = _ctx.Jokes.Find(filter).FirstOrDefaultAsync();

            var userFilter = Builders<AuthUser>.Filter.Eq(u => u.Id, userId);
            var userQuery = _ctx.Users.Find(userFilter).FirstOrDefaultAsync();
            jokeQuery.Wait();
            jokeQuery.Wait();

            var user = userQuery.Result;
            var joke = jokeQuery.Result;

            if (user != null && joke != null)
            {
                user.FavouriteJokes.RemoveAll(j => j.Id == joke.Id);
                var updateQuery = Builders<AuthUser>.Update.Set(u => u.FavouriteJokes, user.FavouriteJokes);
                _ctx.Users.UpdateOneAsync(userFilter, updateQuery).Wait();
            }
        }

        public IEnumerable<Joke> LoadFavourites(Func<UserFavouriteJoke, bool> predicate, out int totalItems)
        {
            var filter = Builders<AuthUser>.Filter.Eq(u => u.Id, AuthUser.Instance.Id);
            var query = _ctx.Users.Find(filter).FirstOrDefaultAsync();
            query.Wait();
            totalItems = query.Result.FavouriteJokes.Count;
            return query.Result.FavouriteJokes;
        }
    }
}
