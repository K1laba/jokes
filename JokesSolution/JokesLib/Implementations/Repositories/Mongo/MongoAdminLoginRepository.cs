﻿using JokesLib.Database;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Implementations.Repositories.Mongo
{
    public class MongoAdminLoginRepository : IAdminLoginRepository
    {
        MongoDbContext _ctx = new MongoDbContext();
        public void Authenticate(string username, string password)
        {
            var builder = Builders<Administrator>.Filter;
            var filter = builder.Eq(i => i.UserName, username.ToLower()) &
                       builder.Eq(i => i.Password, password);
            var query = _ctx.Admins.Find(filter).FirstOrDefaultAsync();
            query.Wait();
            var admin = query.Result;
            if (admin != null)
            {
                Administrator.Instance.Id = admin.Id;
                Administrator.Instance.UserName = admin.UserName;
                Administrator.Instance.IsAuthenticated = true;
            }
        }
    }
}
