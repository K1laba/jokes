﻿using JokesLib.Database;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Implementations.Repositories
{
    public class EntityFacebookAuthenticationRepository : IAuthenticationRepository
    {
        public void Authenticate(AuthUser userInfo)
        {
            if (userInfo == null) { return; }
            using (var db = new JokesDbContext())
            {
                var user = db.Users.AsNoTracking().FirstOrDefault(u => u.FacebookId == userInfo.FacebookId);
                if (user != null) { userInfo.Id = user.Id; }
                db.Users.Attach(userInfo);
                var state = user != null ? System.Data.Entity.EntityState.Modified
                    : System.Data.Entity.EntityState.Added;
                db.Entry(userInfo).State = state;
                db.SaveChanges();
                AuthUser.Instance = userInfo;
                AuthUser.Instance.IsAuthenticated = true;
            }
        }
    }
}
