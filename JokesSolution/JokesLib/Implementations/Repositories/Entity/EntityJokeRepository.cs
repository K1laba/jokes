﻿using JokesLib.Database;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace JokesLib.Implementations.Repositories
{
    public class EntityJokeRepository : IJokeRepository, IDisposable
    {
        private JokesDbContext _context = new JokesDbContext();
        public IEnumerable<Joke> LoadAll(Func<Joke, bool> predicate, out int totalItems)
        {
            totalItems = _context.Jokes.Where(predicate).Count();
            return _context.Jokes
                            .Where(predicate)
                            .OrderByDescending(x => x.PublishDT);
        }
        public Joke Load(Guid id)
        {
            return _context.Jokes.FirstOrDefault(i => i.Id == id);
        }
        public void Add(Joke joke)
        {
            if (AuthUser.Instance.IsAuthenticated) { joke.UserId = AuthUser.Instance.Id; }
            joke.Id = Guid.NewGuid();
            _context.Jokes.Add(joke);
            _context.SaveChanges();
        }
        public void Edit(Joke joke)
        {
            var itemToEdit = _context.Jokes.FirstOrDefault(i => i.Id == joke.Id);
            if (itemToEdit != null)
            {
                itemToEdit = joke;
                _context.Entry(itemToEdit).State = System.Data.Entity.EntityState.Modified;
            }
            _context.SaveChanges();
        }
        public void Delete(Guid id)
        {
            var itemToDelete = _context.Jokes.FirstOrDefault(i => i.Id == id);

            var favJokesToDelete = _context.UserFavouriteJokes.Where(i => i.JokeId == itemToDelete.Id);
            if (favJokesToDelete != null) { _context.UserFavouriteJokes.RemoveRange(favJokesToDelete); }

            var ratedJokesToDelete = _context.UsersRatedJokes.Where(i => i.JokeId == itemToDelete.Id);
            if (ratedJokesToDelete != null) { _context.UsersRatedJokes.RemoveRange(ratedJokesToDelete); }

            if (itemToDelete != null) { _context.Jokes.Remove(itemToDelete); }
            
            _context.SaveChanges();
        }
        public void Publish(Guid id)
        {
            var itemToPublish = _context.Jokes.FirstOrDefault(i => i.Id == id);
            if (itemToPublish != null)
            {
                itemToPublish.Status = JokeStatus.Published;
                itemToPublish.PublishDT = DateTime.Now;
                _context.SaveChanges();
            }
        }
        public Joke Rate(Guid jokeId, int rateValue)
        {
            var itemToRate = _context.Jokes.FirstOrDefault(i => i.Id == jokeId);
            if (itemToRate != null)
            {
                var currentRate = _context.UsersRatedJokes.FirstOrDefault(i => i.AuthUserId == AuthUser.Instance.Id
                && i.JokeId == itemToRate.Id);
                if (currentRate == null)
                {
                    currentRate = new UserRatedJoke();
                    itemToRate.TotalVotes++;
                }
                else { itemToRate.RateValue -= currentRate.RateValue; }
                itemToRate.RateValue += rateValue;

                currentRate.RateValue = rateValue;
                currentRate.RateDT = DateTime.Now;
                currentRate.AuthUserId = AuthUser.Instance.Id;
                currentRate.JokeId = itemToRate.Id;
                _context.Entry(currentRate).State = currentRate.Id != default(Guid) ? EntityState.Modified : EntityState.Added;
                _context.SaveChanges();
                return itemToRate;
            }
            return new Joke();
        }
        public void AddToFavourites(Guid jokeId, Guid userId)
        {
            var jokeToAdd = _context.Jokes.FirstOrDefault(i => i.Id == jokeId && i.Status == JokeStatus.Published);
            bool alreadyAdded = _context.UserFavouriteJokes.FirstOrDefault(j => j.JokeId == jokeToAdd.Id && j.UserId == userId) != null;
            if (jokeToAdd != null && !alreadyAdded)
            {
                _context.UserFavouriteJokes.Add(new UserFavouriteJoke() { UserId = userId, JokeId = jokeToAdd.Id, AddedToFavouritesDT = DateTime.Now });
                _context.SaveChanges();
            }
        }
        public void DeleteFromFavourites(Guid jokeId, Guid userId)
        {
            var joke = _context.Jokes.FirstOrDefault(j => j.Id == jokeId);
            var jokeToDelete = _context.UserFavouriteJokes.FirstOrDefault(j => j.JokeId == joke.Id && j.UserId == userId);
            if (jokeToDelete != null)
            {
                _context.UserFavouriteJokes.Remove(jokeToDelete);
                _context.SaveChanges();
            }
        }

        public IEnumerable<Joke> LoadFavourites(Func<UserFavouriteJoke, bool> predicate, out int totalItems)
        {
            totalItems = _context.UserFavouriteJokes.Where(predicate).Count();
            return _context.UserFavouriteJokes
                .Include(i => i.Joke)
                .Where(predicate)
                .OrderByDescending(f => f.AddedToFavouritesDT)
                .Select(f => f.Joke)
                .OrderByDescending(f => f.RateValue);
        }
        public void Dispose()
        {
            _context.Dispose();
        }


        public IEnumerable<UserRatedJoke> LoadRated(Func<UserRatedJoke, bool> predicate, out int totalItems)
        {
            totalItems = _context.UsersRatedJokes.Include(r => r.Joke).Where(predicate).Count();
            return _context.UsersRatedJokes
                .Include(i => i.Joke)
                .Where(predicate);
        }
    }
}
