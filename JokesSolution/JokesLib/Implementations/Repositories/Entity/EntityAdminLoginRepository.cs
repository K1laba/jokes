﻿using JokesLib.Database;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Implementations.Repositories
{
    public class EntityAdminLoginRepository : IAdminLoginRepository, IDisposable
    {
        private JokesDbContext _context = new JokesDbContext();
        public void Authenticate(string username, string password)
        {
            var admin = _context.Admins.FirstOrDefault(i => i.UserName == username && i.Password == password);
            if (admin != null)
            {
                Administrator.Instance.Id = admin.Id;
                Administrator.Instance.UserName = admin.UserName;
                Administrator.Instance.IsAuthenticated = true;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
