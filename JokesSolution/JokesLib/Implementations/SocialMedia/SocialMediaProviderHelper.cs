﻿using JokesLib.Helpers;
using JokesLib.Implementations.Repositories;
//using JokesLib.Implementations.Repositories.Mongo;
using JokesLib.Interfaces.Repositories;
using JokesLib.Interfaces.SocialMedia;
using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Implementations.SocialMedia
{
    public class SocialMediaProviderHelper : ISocialMediaProviderHelper
    {
        private string _fbAppId
        {
            get { return ConfigurationManager.AppSettings["FacebookAppId"]; }
        }
        private string _fbAppSecret
        {
            get { return ConfigurationManager.AppSettings["FacebookAppSecret"]; }
        }
        private string _gPlusAppId
        {
            get { return ConfigurationManager.AppSettings["GoogleAppId"]; }
        }
        private string _gPlusAppSecret
        {
            get { return ConfigurationManager.AppSettings["GoogleAppSecret"]; }
        }
        private Dictionary<Models.SocialMediaType, SocialMediaAuthenticationProviderBase> _authProviders
        {
            get
            {
                string pathFormat = "/login/AuthenticationCallback?mediaType=";
                var providers = new Dictionary<Models.SocialMediaType, SocialMediaAuthenticationProviderBase>();
                providers.Add(SocialMediaType.Facebook, new FacebookAuthenticationProvider(_fbAppId, _fbAppSecret,
                            String.Format("{0}{1}{2}", Helper.GetCurrentDomain(), pathFormat, SocialMediaType.Facebook)));
                providers.Add(SocialMediaType.GooglePlus, new GooglePlusAuthenticationProvider(_gPlusAppId, _gPlusAppSecret,
                                String.Format("{0}{1}{2}", Helper.GetCurrentDomain(), pathFormat, SocialMediaType.GooglePlus)));
                return providers;
            }
        }
        private Dictionary<SocialMediaType, IAuthenticationRepository> _authRepos
        {
            get
            {
                var repositories = new Dictionary<SocialMediaType, IAuthenticationRepository>();
                repositories.Add(SocialMediaType.Facebook, new EntityFacebookAuthenticationRepository());
                repositories.Add(SocialMediaType.GooglePlus, new EntityGooglePlusAuthenticationRepository());
                return repositories;
            }
        }
        public SocialMediaAuthenticationProviderBase GetAuthenticationProvider(SocialMediaType mediaType)
        {
            return this._authProviders[mediaType];
        }
        public IAuthenticationRepository GetAuthenticationRepository(SocialMediaType mediaType)
        {
            return this._authRepos[mediaType];
        }
    }
}
