﻿using JokesLib.Helpers;
using JokesLib.Interfaces.SocialMedia;
using JokesLib.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JokesLib.Implementations.SocialMedia
{
    public class GooglePlusAuthenticationProvider : SocialMediaAuthenticationProviderBase
    {
        public GooglePlusAuthenticationProvider(string appId, string secretKey, string callbackUrl) : base(appId, secretKey, callbackUrl) { }
        public override string GetRedirectUrl()
        {
            string googleState = new Guid().ToString();
            HttpContext.Current.Session["googlestate"] = googleState;
            string url = "https://accounts.google.com/o/oauth2/auth";
            Dictionary<string, string> queries = new Dictionary<string, string>();
            queries.Add("client_id", _appId);
            queries.Add("response_type", "code");
            queries.Add("scope", "openid%20email");
            queries.Add("redirect_uri", _callbackUrl);
            queries.Add("state", googleState);
            return url + Helper.BuildQuery(queries);
        }

        public override AuthUser GetUserInfo()
        {
            if (!IsAntiForgeryTokenValid()) { return null; }
            IRestResponse response = RequestAccessToken();
            dynamic tokenInfo = Newtonsoft.Json.JsonConvert.DeserializeObject(response.Content);
            IRestResponse userInfoResponse = RequestUserInfo(tokenInfo.access_token.ToString());
            dynamic userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject(userInfoResponse.Content);
            return new AuthUser()
            {
                FirstName = userInfo.given_name,
                LastName = userInfo.family_name,
                Email = userInfo.email,
                GooglePlusId = userInfo.id
            };
        }

        private IRestResponse RequestAccessToken()
        {
            var client = new RestClient("https://www.googleapis.com");
            var request = new RestRequest("/oauth2/v3/token", Method.POST);
            request.AddParameter("code", HttpContext.Current.Request["code"]);
            request.AddParameter("client_id", _appId);
            request.AddParameter("client_secret", _secretKey);
            request.AddParameter("redirect_uri", _callbackUrl);
            request.AddParameter("grant_type", "authorization_code");

            IRestResponse response = client.Execute(request);
            return response;
        }
        private IRestResponse RequestUserInfo(string token)
        {
            var client = new RestClient("https://www.googleapis.com");
            var request = new RestRequest("/oauth2/v1/userinfo", Method.GET);
            request.AddParameter("alt", "json");
            request.AddParameter("access_token", token);
            IRestResponse response = client.Execute(request);
            return response;
        }

        private bool IsAntiForgeryTokenValid()
        {
            return HttpContext.Current.Request["state"] != null && HttpContext.Current.Session["googlestate"] != null &&
                            HttpContext.Current.Request["state"].ToString() == HttpContext.Current.Session["googlestate"].ToString();
        }
    }
}
