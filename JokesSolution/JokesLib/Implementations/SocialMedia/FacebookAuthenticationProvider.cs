﻿using Facebook;
using JokesLib.Interfaces.SocialMedia;
using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JokesLib.Implementations.SocialMedia
{
    public class FacebookAuthenticationProvider : SocialMediaAuthenticationProviderBase
    {
        public FacebookAuthenticationProvider(string appId, string secretKey, string callbackUrl) : base(appId, secretKey, callbackUrl) { }
        public override string GetRedirectUrl()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = _appId,
                redirect_uri = _callbackUrl,
                response_type = "code",
                scope = "email"
            });
            return loginUrl.AbsoluteUri;
        }

        public override AuthUser GetUserInfo()
        {
            var fb = new FacebookClient();
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = _appId,
                client_secret = _secretKey,
                redirect_uri = _callbackUrl,
                code = HttpContext.Current.Request["code"]
            });

            var accessToken = result.access_token;
            fb.AccessToken = accessToken;

            dynamic me = fb.Get("me?fields=first_name,last_name,id,email");

            return new AuthUser()
            {
                Email = me.email,
                FacebookId = me.id,
                FirstName = me.first_name,
                LastName = me.last_name,
            };
        }
    }
}
