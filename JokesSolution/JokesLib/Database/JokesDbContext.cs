﻿using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Database
{
    public class JokesDbContext : DbContext
    {
        public JokesDbContext()
            : base("name=JokesDbContext")
        {
            
        }
        public DbSet<Joke> Jokes { get; set; }
        public DbSet<AuthUser> Users { get; set; }
        public DbSet<UserRatedJoke> UsersRatedJokes { get; set; }
        public DbSet<UserFavouriteJoke> UserFavouriteJokes { get; set; }
        public DbSet<Administrator> Admins { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            //modelBuilder.Entity<Joke>()
            //.HasOptional(c => c.User)
            //.WithMany()
            //.WillCascadeOnDelete(false);
        }
    }
}
