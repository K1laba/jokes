﻿using JokesLib.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Database
{
    public class MongoDbContext
    {
        private IMongoDatabase db;
        public MongoDbContext()
        {
            var mongoClient = new MongoClient(ConfigurationManager.AppSettings["MongoConString"]);
            db = mongoClient.GetDatabase(ConfigurationManager.AppSettings["MongoDbName"]);
        }
        public IMongoCollection<Administrator> Admins
        {
            get { return db.GetCollection<Administrator>("Administrators"); }
        }
        public IMongoCollection<AuthUser> Users
        {
            get { return db.GetCollection<AuthUser>("Users"); }
        }
        public IMongoCollection<Joke> Jokes
        {
            get { return db.GetCollection<Joke>("Jokes"); }
        }
    }
}
