﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Models
{
    public enum SocialMediaType
    {
        Facebook = 1,
        GooglePlus = 2
    }
}
