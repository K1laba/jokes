﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Models
{
    public class UserFavouriteJoke
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid JokeId { get; set; }
        [ForeignKey("JokeId")]
        public virtual Joke Joke { get; set; }
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual AuthUser User { get; set; }
        public DateTime AddedToFavouritesDT { get; set; }
    }
}
