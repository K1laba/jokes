﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JokesLib.Models
{
    public class Administrator
    {
        public static Administrator Instance
        {
            get
            {
                if (HttpContext.Current.Session["admin"] == null)
                {
                    HttpContext.Current.Session["admin"] = new Administrator();
                }
                return (Administrator)HttpContext.Current.Session["admin"];
            }
            set
            {
                HttpContext.Current.Session["admin"] = value;
            }
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [BsonElement("_id")]
        public Guid Id { get; set; }
        public string UserName { get; set; }
        [NotMapped]
        [BsonIgnore]
        public bool IsAuthenticated { get; set; }
        public string Password { get; set; }
    }
}
