﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Models
{
    public class Joke
    {
        private DateTime _createDT = DateTime.Now;
        private DateTime _publishDT = DateTime.Now;
        private JokeStatus _status = (JokeStatus)Convert.ToInt32(ConfigurationManager.AppSettings["AutomaticPublishEnabled"]);
        [BsonElement("_id")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Description { get; set; }
        public DateTime CreateDT { get { return _createDT; } set { _createDT = value; } }
        public DateTime PublishDT { get { return _publishDT; } set { _publishDT = value; } }
        public JokeStatus Status { get { return _status; } set { _status = value; } }
        public int TotalVotes { get; set; }
        public int RateValue { get; set; }
        public Guid? UserId { get; set; }
        [ForeignKey("UserId")]
        public AuthUser User { get; set; }
    }
    public enum JokeStatus
    {
        PublishRequired = 0,
        Published = 1
    }
}
