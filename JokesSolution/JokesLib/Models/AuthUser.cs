﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JokesLib.Models
{
    public class AuthUser
    {
        public static AuthUser Instance
        {
            get
            {
                if (HttpContext.Current.Session["authUser"] == null)
                {
                    HttpContext.Current.Session["authUser"] = new AuthUser();
                }
                return (AuthUser)HttpContext.Current.Session["authUser"];
            }
            set
            {
                HttpContext.Current.Session["authUser"] = value;
            }
        }
        public AuthUser()
        {
            RatedJokes = new List<UserRatedJoke>();
            FavouriteJokes = new List<Joke>();
        }
        [BsonElement("_id")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string FacebookId { get; set; }
        public string GooglePlusId { get; set; }
        [NotMapped]
        [BsonIgnore]
        public bool IsAuthenticated { get; set; }
        [NotMapped]
        public virtual List<UserRatedJoke> RatedJokes { get; set; }
        [NotMapped]
        public virtual List<Joke> FavouriteJokes { get; set; }

    }
}
