﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Models
{
    public class PagerInfo
    {
        public string UrlTemplate { get; private set; }
        public int ItemsPerPage { get; private set; }
        public int TotalItems { get; private set; }
        public int CurrentPage { get; private set; }
        public PagerInfo(int itemsPerPage, int totalItems, int currentPage, string urlTemplate)
        {
            ItemsPerPage = itemsPerPage;
            TotalItems = totalItems;
            CurrentPage = currentPage;
            UrlTemplate = String.IsNullOrEmpty(urlTemplate) ? "?p={0}" : urlTemplate;
        }
    }
}
