﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesLib.Models
{
    public class UserRatedJoke
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [BsonIgnore]
        public Guid AuthUserId { get; set; }
        [ForeignKey("AuthUserId")]
        [BsonIgnore]
        public virtual AuthUser User { get; set; }
        [BsonIgnore]
        public Guid JokeId { get; set; }
        [ForeignKey("JokeId")]
        public virtual Joke Joke { get; set; }
        public int RateValue { get; set; }
        public DateTime RateDT { get; set; }
    }
}
