﻿using JokesLib.Helpers;
using JokesLib.Implementations.SocialMedia;
using JokesLib.Interfaces.Repositories;
using JokesLib.Interfaces.SocialMedia;
using JokesLib.Models;
using JokesWebsite.Attributes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JokesWebsite.Controllers
{
    public class LoginController : Controller
    {
        private ISocialMediaProviderHelper _socialMediaHelper = null;
        private IAuthenticationRepository _authRepo { get; set; }
        private SocialMediaAuthenticationProviderBase _authProvider { get; set; }
        public LoginController(ISocialMediaProviderHelper socialMediaHelper)
        {
            if (socialMediaHelper == null) { throw new ArgumentNullException("socialMediaHelper"); }
            _socialMediaHelper = socialMediaHelper;
        }
        public ActionResult Authenticate(SocialMediaType mediaType)
        {
            _authProvider = _socialMediaHelper.GetAuthenticationProvider(mediaType);
            return Redirect(_authProvider.GetRedirectUrl());
        }
        public ActionResult AuthenticationCallback(SocialMediaType mediaType)
        {
            _authProvider = _socialMediaHelper.GetAuthenticationProvider(mediaType);
            _authRepo = _socialMediaHelper.GetAuthenticationRepository(mediaType);
            var userInfo = _authProvider.GetUserInfo();
            _authRepo.Authenticate(userInfo);
            return RedirectToAction("Index", "Jokes");
        }
        [AuthorizationRequired]
        public ActionResult SignOut()
        {
            AuthUser.Instance = new AuthUser() { IsAuthenticated = false };
            return RedirectToAction("Index", "Jokes");
        }

    }
}
