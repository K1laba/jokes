﻿using JokesLib.Interfaces;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using JokesWebsite.Attributes;
using JokesWebsite.Interfaces;
using JokesWebsite.Models;
using JokesWebsite.Models.VM;
using ModelMapping;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LinqDyn;

namespace JokesWebsite.Controllers
{
    public class JokesController : Controller
    {
        private int _itemsPerPage
        {
            get
            {
                int count;
                if (!int.TryParse(ConfigurationManager.AppSettings["JokesPerPage"], out count)) { count = 10; }
                return count;
            }
        }
        private IJokeRepository _repo = null;
        private IModelMapper<Joke, JokeVM> _mapper = null;
        private ICaptcha _captcha = null;
        public int CurrentPage
        {
            get
            {
                int currentPage;
                if (Request["p"] == null || !int.TryParse(Request["p"], out currentPage))
                {
                    currentPage = 1;
                }
                return currentPage;
            }
        }

        public JokesController(IJokeRepository repo, IModelMapper<Joke, JokeVM> mapper, ICaptcha captcha)
        {
            if (repo == null) { throw new ArgumentNullException("repo"); }
            if (mapper == null) { throw new ArgumentNullException("mapper"); }
            if (captcha == null) { throw new ArgumentNullException("captcha"); }
            _repo = repo;
            _mapper = mapper;
            _captcha = captcha;

            if (AuthUser.Instance.IsAuthenticated)
            {
                int totalFavourites;
                int totalRated;
                AuthUser.Instance.RatedJokes = _repo.LoadRated(i => i.AuthUserId == AuthUser.Instance.Id, out totalRated).ToList();
                AuthUser.Instance.FavouriteJokes = _repo.LoadFavourites(u => u.UserId == AuthUser.Instance.Id, out totalFavourites).ToList();
            }
        }
        public ActionResult Index(string sortBy = "PublishDT")
        {
            var model = new List<JokeVM>();
            int totalItems;
            var jokes = _repo.LoadAll(x => x.Status == JokeStatus.Published, out totalItems)
                            .OrderByDescending(sortBy)
                            .Skip(_itemsPerPage * (CurrentPage - 1))
                            .Take(_itemsPerPage)
                            .ToList();
            jokes.ForEach(j => model.Add(_mapper.MapToViewModel(j)));

            ViewBag.SortType = sortBy;
            ViewBag.PagerInfo = new PagerInfo(_itemsPerPage, totalItems, CurrentPage, "?p={0}");
            return View(model);
        }
        public ActionResult Single(Guid id)
        {
            var model = new List<JokeVM>();
            var joke = _repo.Load(id);
            if (joke == null || joke.Status != JokeStatus.Published) { return View("Index", model); }
            model.Add(_mapper.MapToViewModel(joke)); ;
            return View("Index", model);
        }
        public ActionResult Create()
        {
            return View(new JokeVM());
        }
        [HttpPost]
        public ActionResult Create(JokeVM joke)
        {
            if (!_captcha.IsValid()) { ModelState.AddModelError("invalidCaptcha", "გთხოვთ მონიშნოთ რომ არ ხართ ბოტი"); }
            if (!ModelState.IsValid) { return View(); }

            joke.Description = joke.Description.Replace("\n", "<br/>");
            var jokeToAdd = _mapper.MapToEntity(joke);
            _repo.Add(jokeToAdd);
            return View("Completed");
        }
        [AuthorizationRequired]
        public JsonResult Rate(Guid jokeId, int rateValue)
        {
            rateValue = Math.Max(-1, rateValue);
            rateValue = Math.Min(1, rateValue);
            var joke = _repo.Rate(jokeId, rateValue);
            return Json(new
            {
                JokeId = joke.Id,
                TotalVotes = joke.TotalVotes,
                RateValue = joke.RateValue
            }, JsonRequestBehavior.AllowGet);
        }
        [AuthorizationRequired]
        public JsonResult AddToFavourites(Guid jokeId)
        {
            _repo.AddToFavourites(jokeId, AuthUser.Instance.Id);
            return Json(new { isAdded = true, jokeId = jokeId }, JsonRequestBehavior.AllowGet);
        }
        [AuthorizationRequired]
        public JsonResult RemoveFromFavourites(Guid jokeId)
        {
            _repo.DeleteFromFavourites(jokeId, AuthUser.Instance.Id);
            return Json(new { isRemoved = true }, JsonRequestBehavior.AllowGet);
        }
        [AuthorizationRequired]
        public ActionResult Favourites()
        {
            var model = new List<JokeVM>();
            int totalItems;
            var jokes = _repo.LoadFavourites(x => x.UserId == AuthUser.Instance.Id, out totalItems)
                            .Skip(_itemsPerPage * (CurrentPage - 1))
                            .Take(_itemsPerPage)
                            .Where(j => j.Status == JokeStatus.Published)
                            .ToList();
            jokes.ForEach(j => model.Add(_mapper.MapToViewModel(j)));
            ViewBag.PagerInfo = new PagerInfo(_itemsPerPage, totalItems, CurrentPage, "?p={0}");
            return View("Index", model);
        }
    }
}
