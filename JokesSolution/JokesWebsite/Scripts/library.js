﻿var Joke = {
    IsAuthorized: false,
    Rate: function (jokeId, rate) {
        if (!this.CheckIfAuthorized()) { return; }
        this.RequestProcessor("/Jokes/Rate", { jokeId: jokeId, rateValue: rate }, function (response) {
            var selector = $('[data-jokeId=' + response.JokeId + ']');
            var thumbToDisableSelector = rate > 0 ? $('[data-thumb-up=' + response.JokeId + ']') : $('[data-thumb-down=' + response.JokeId + ']');
            var thumbToEnableSelector = rate < 0 ? $('[data-thumb-up=' + response.JokeId + ']') : $('[data-thumb-down=' + response.JokeId + ']');
            var disableClassName = 'disabled';
            selector.html(response.RateValue);
            thumbToDisableSelector.removeClass(disableClassName).addClass(disableClassName);
            thumbToEnableSelector.removeClass(disableClassName);
        });
    },
    CheckIfAuthorized: function () {
        if (!this.IsAuthorized) {
            this.ShowAuthorizationRequiredPopup();
            return false;
        }
        return true;
    },
    ShowAuthorizationRequiredPopup: function () {
        $('#askToLogin').modal('show');
    },
    Sort: function (sortType) {
        window.location.href = window.location.origin + '/' + sortType
    },
    AddToFavourites: function (jokeId) {
        if (!this.CheckIfAuthorized()) { return; }
        if ($('[data-fav-jokeId=' + jokeId + ']').hasClass('disabled')) { return; }
        this.RequestProcessor("/Jokes/AddToFavourites", { jokeId: jokeId }, function (response) {
            if (response && response.isAdded) {
                var disableClassName = 'disabled';
                $('[data-fav-jokeId=' + response.jokeId + ']').removeClass(disableClassName).addClass(disableClassName);
            }
        });
    },
    RemoveFromFavourites: function (jokeId) {
        this.RequestProcessor("/Jokes/RemoveFromFavourites", { jokeId: jokeId }, function (response) {
            if (response && response.isRemoved) {
                $('[data-jokecontainer=' + jokeId + ']').remove();
            }
        });
    },
    RequestProcessor: function (url, input, successCallback) {
        $.ajax({
            type: "GET",
            url: url,
            data: input,
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: successCallback
        });
    }
};