﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JokesWebsite.Interfaces
{
    public interface ICaptcha
    {
        bool IsValid();
    }
}
