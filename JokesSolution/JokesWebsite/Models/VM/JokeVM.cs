﻿using JokesLib.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JokesWebsite.Models.VM
{
    public class JokeVM
    {
        private DateTime _publishDT = DateTime.Now;
        public Guid Id { get; set; }
        [Required(ErrorMessage = "გთხოვთ შეავსოთ ველი")]
        public string Description { get; set; }
        public DateTime PublishDT { get { return _publishDT; } set { _publishDT = value; } }
        public int TotalVotes { get; set; }
        public int RateValue { get; set; }
        public bool UserHasAlreadyRated
        {
            get
            {
                if (!AuthUser.Instance.IsAuthenticated) { return false; }
                return AuthUser.Instance.RatedJokes.FirstOrDefault(j => j.Joke.Id == Id) != null;
            }
        }
        public bool IsUserFavouriteJoke
        {
            get
            {
                if (!AuthUser.Instance.IsAuthenticated) { return false; }
                return AuthUser.Instance.FavouriteJokes.FirstOrDefault(j => j.Id == Id) != null;
            }
        }
        public RateType RateType
        {
            get
            {
                return this.UserHasAlreadyRated ? 
                    (RateType)AuthUser.Instance.RatedJokes.FirstOrDefault(j => j.Joke.Id == Id).RateValue 
                    : RateType.None;
            }
        }
    }
    public enum RateType
    {
        DownVote = -1,
        None = 0,
        UpVote = 1
    }
}