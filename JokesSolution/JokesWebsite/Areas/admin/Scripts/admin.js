﻿var admin = {
    Publish: function (jokeId) {
        this.RequestProcessor('home/publish', { jokeId: jokeId }, function (response) {
            if (response && response.status == 200) {
                $('[data-joke-approve=' + jokeId + ']').removeClass('disabled').addClass('disabled');
                setTimeout(function () { $('[data-jokewrapper=' + jokeId + ']').remove(); }, 1000);
            }
        });
    },
    Delete: function (jokeId) {
        this.RequestProcessor('/home/delete', { jokeId: jokeId }, function (response) {
            if (response && response.status == 200) {
                $('[data-joke-remove=' + jokeId + ']').removeClass('disabled').addClass('disabled');
                setTimeout(function () { $('[data-jokewrapper=' + jokeId + ']').remove(); }, 1000);
            }
        });
    },
    RequestProcessor: function (url, input, successCallback) {
        $.ajax({
            type: "GET",
            url: url,
            data: input,
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: successCallback
        });
    }
}