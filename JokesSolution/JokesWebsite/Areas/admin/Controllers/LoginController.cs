﻿using JokesLib.Helpers;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using JokesWebsite.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JokesWebsite.Areas.admin.Controllers
{
    public class LoginController : Controller
    {
        private IAdminLoginRepository _repo = null;
        private ICaptcha _captccha = null;
        public LoginController(IAdminLoginRepository repo, ICaptcha captcha)
        {
            if (repo == null) { throw new ArgumentNullException("repo"); }
            if (captcha == null) { throw new ArgumentNullException("captcha"); }
            _repo = repo;
            _captccha = captcha;
        }
        public ActionResult Index()
        {
            if (Administrator.Instance.IsAuthenticated) { return RedirectToAction("Index", "Home"); }
            return View();
        }
        [HttpPost]
        public ActionResult Index(string username, string password)
        {
            if (!HasValidInput(username, password)) { ModelState.AddModelError("invalidInput", "სახელი და პაროლი აუცილებელი ველებია"); }
            if (!_captccha.IsValid()) { ModelState.AddModelError("invalidcaptcha", "გთხოვთ მონიშნოთ რომ არ ხართ ბოტი"); }
            if (!ModelState.IsValid) { return View(); }
            _repo.Authenticate(username, Helper.EncryptMD5(password));
            if (Administrator.Instance.IsAuthenticated) { return RedirectToAction("Index", "Home"); }
            return View();
        }
        private bool HasValidInput(string username, string password)
        {
            return !String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password);
        }

        public ActionResult SignOut()
        {
            Administrator.Instance = new Administrator();
            return RedirectToAction("Index");
        }
    }
}
