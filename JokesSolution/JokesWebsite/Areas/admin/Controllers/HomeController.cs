﻿using JokesLib.Interfaces;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using JokesWebsite.Areas.admin.Attributes;
using JokesWebsite.Models.VM;
using ModelMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace JokesWebsite.Areas.admin.Controllers
{
    [Administrator]
    public class HomeController : Controller
    {
        private IJokeRepository _repo = null;
        private IModelMapper<Joke, JokeVM> _mapper = null;
        public HomeController(IJokeRepository repo, IModelMapper<Joke, JokeVM> mapper)
        {
            if (repo == null) { throw new ArgumentNullException("repo"); }
            if (mapper == null) { throw new ArgumentNullException("mapper"); }
            _repo = repo;
            _mapper = mapper;
        }
        public ActionResult Index()
        {
            int totalCount;
            var jokes = _repo.LoadAll(i => i.Status == JokeStatus.PublishRequired, out totalCount).ToList();
            var model = new List<JokeVM>();
            jokes.ForEach(j => model.Add(_mapper.MapToViewModel(j)));
            return View(model);
        }

        public JsonResult Delete(Guid jokeId)
        {
            _repo.Delete(jokeId);
            return Json(new { jokeId = jokeId, status = HttpStatusCode.OK }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Publish(Guid jokeId)
        {
            _repo.Publish(jokeId);
            return Json(new { jokeId = jokeId, status = HttpStatusCode.OK }, JsonRequestBehavior.AllowGet);
        }
    }
}
