[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(JokesWebsite.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(JokesWebsite.App_Start.NinjectWebCommon), "Stop")]

namespace JokesWebsite.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using JokesLib.Interfaces.Repositories;
    using JokesLib.Implementations.Repositories;
    using JokesLib.Models;
    using JokesWebsite.Models.VM;
    using JokesLib.Interfaces;
    using JokesWebsite.Implementations;
    using JokesWebsite.Interfaces;
    using JokesLib.Interfaces.SocialMedia;
    using JokesLib.Implementations.SocialMedia;
    using JokesLib.Implementations.Repositories.Mongo;
    using ModelMapping;
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IJokeRepository>().To<EntityJokeRepository>();
            kernel.Bind<IAdminLoginRepository>().To<EntityAdminLoginRepository>();
            kernel.Bind<IModelMapper<Joke, JokeVM>>().To<ModelMapper<Joke, JokeVM>>();
            kernel.Bind<ICaptcha>().To<GooglaCaptcha>();
            kernel.Bind<ISocialMediaProviderHelper>().To<SocialMediaProviderHelper>();
        }        
    }
}
