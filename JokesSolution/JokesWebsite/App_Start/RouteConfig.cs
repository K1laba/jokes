﻿using JokesWebsite.Models.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace JokesWebsite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.LowercaseUrls = true;

            routes.MapRoute(
                name: "SortByNew",
                url: "New",
                defaults: new { controller = "Jokes", action = "Index", sortBy = "PublishDT" }
            );
            routes.MapRoute(
                name: "SortByPopular",
                url: "popular",
                defaults: new { controller = "Jokes", action = "Index", sortBy = "RateValue" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Jokes", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "JokesWebsite.Controllers" }
            );
        }
    }
}