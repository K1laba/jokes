﻿using JokesWebsite.Interfaces;
using JokesWebsite.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace JokesWebsite.Implementations
{
    public class GooglaCaptcha : ICaptcha
    {
        private string secretKey { get { return ConfigurationManager.AppSettings["GoogleCaptchaSeccretKey"]; } }
        public bool IsValid()
        {
            try
            {
                var response = HttpContext.Current.Request["g-recaptcha-response"];

                var client = new System.Net.WebClient();
                var reply =
                    client.DownloadString(
                        string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));

                var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);
                return captchaResponse.Success;
            }
            catch (Exception ex) { return false; }
        }
    }
}