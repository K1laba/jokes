﻿using JokesLib.Models;
using JokesWebsite.Models.VM;
using JokesWebsite.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace JokesWebsite.Tests.VMTests
{
    public class JokeVMTests
    {
        public JokeVMTests()
        {
            Helper.SetupFakeHttpContext();
        }

        [Fact]
        public void IsRatedByUser_WhenUserHasRatedSelectedJoke_ShouldReturnTrue()
        {
            Guid anyguid = Guid.NewGuid();
            AuthUser.Instance.IsAuthenticated = true;
            AuthUser.Instance.RatedJokes.Add(new UserRatedJoke() { Joke = new Joke() { Id = anyguid } });
            Assert.True(new JokeVM() { Id = anyguid }.UserHasAlreadyRated == true);
        }
    }
}
