﻿using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JokesWebsite.Tests.Helpers
{
    public class Helper
    {
        public static void SetupFakeHttpContext()
        {
            var httpRequest = new HttpRequest("", "https://google.com/", "");
            var stringWriter = new System.IO.StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponce);

            var sessionContainer = new System.Web.SessionState.HttpSessionStateContainer("id", new System.Web.SessionState.SessionStateItemCollection(),
                                                                 new HttpStaticObjectsCollection(), 10, true,
                                                                 HttpCookieMode.AutoDetect,
                                                                 System.Web.SessionState.SessionStateMode.InProc, false);

            httpContext.Items["AspSession"] = typeof(System.Web.SessionState.HttpSessionState).GetConstructor(
                                                     System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance,
                                                     null, System.Reflection.CallingConventions.Standard,
                                                     new[] { typeof(System.Web.SessionState.HttpSessionStateContainer) },
                                                     null)
                                                .Invoke(new object[] { sessionContainer });

            HttpContext.Current = httpContext;
        }
    }
}
