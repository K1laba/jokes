﻿using JokesLib.Implementations.SocialMedia;
using JokesLib.Interfaces.Repositories;
using JokesLib.Interfaces.SocialMedia;
using JokesLib.Models;
using JokesWebsite.Controllers;
using JokesWebsite.Tests.Helpers;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Xunit;

namespace JokesWebsite.Tests.Controllers
{
    public class LoginControllerTests
    {
        private LoginController _loginController = null;
        private IAuthenticationRepository _repo = null;
        private ISocialMediaProviderHelper _mediaProviderHelper = null;
        private SocialMediaAuthenticationProviderBase _currentAuthProvider = null;
        public LoginControllerTests()
        {
            _repo = Substitute.For<IAuthenticationRepository>();
            _mediaProviderHelper = Substitute.For<ISocialMediaProviderHelper>();
            _loginController = new LoginController(_mediaProviderHelper);
            Helper.SetupFakeHttpContext();
            _loginController.ControllerContext = new ControllerContext(Substitute.For<HttpContextBase>(), new RouteData(), _loginController);
            _loginController.Url = Substitute.For<UrlHelper>(HttpContext.Current.Request.RequestContext);

            _currentAuthProvider = Substitute.For<SocialMediaAuthenticationProviderBase>("", "", "");
            _currentAuthProvider.GetRedirectUrl().Returns((x) => "url");
            _currentAuthProvider.GetUserInfo().Returns(x => new AuthUser());

            _mediaProviderHelper.GetAuthenticationProvider(SocialMediaType.Facebook).Returns(_currentAuthProvider);
            _mediaProviderHelper.GetAuthenticationRepository(SocialMediaType.Facebook).Returns(_repo);
        }
        [Fact]
        public void Constructor_WhenCallsWithNullParameter_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new LoginController(null));
        }
        [Fact]
        public void Constructor_WhenCallsWithValidParameter_ShouldNotThrowException()
        {
            var ex = Record.Exception(() => new LoginController(_mediaProviderHelper));
            Assert.Null(ex);
        }
        [Fact]
        public void Logout_WhenCalls_ShouldLogout()
        {
            AuthUser.Instance.IsAuthenticated = true;
            _loginController.SignOut();
            Assert.True(AuthUser.Instance.IsAuthenticated == false);
        }
        [Fact]
        public void Authenticate_WhenReceivedCall_ShouldRedirectToAuthenticationProvider()
        {
            _loginController.Authenticate(SocialMediaType.Facebook);
            _currentAuthProvider.ReceivedWithAnyArgs().GetRedirectUrl();
        }
        [Fact]
        public void AuthenticateCallback_WhenReceivedCall_ShouldGetUserInfo()
        {
            _loginController.AuthenticationCallback(SocialMediaType.Facebook);
            _currentAuthProvider.ReceivedWithAnyArgs().GetUserInfo();
        }
        [Fact]
        public void AuthenticateCallback_WhenReceivedCall_ShouldAuthenticate()
        {
            var userInfo = new AuthUser();
            _currentAuthProvider.GetUserInfo().Returns((x) => userInfo);
            _loginController.AuthenticationCallback(SocialMediaType.Facebook);
            _repo.Received().Authenticate(userInfo);
        }
    }
}
