﻿using JokesLib.Interfaces;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using JokesWebsite.Controllers;
using JokesWebsite.Interfaces;
using JokesWebsite.Models.VM;
using JokesWebsite.Tests.Helpers;
using ModelMapping;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Xunit;

namespace JokesWebsite.Tests.Controllers
{
    public class JokesControllerTests
    {
        private IJokeRepository _repo = null;
        private IModelMapper<Joke, JokeVM> _mapper = null;
        private ICaptcha _captcha = null;
        private JokesController _jokesController = null;

        private List<Joke> _fakeJokes = null;
        private Joke _fakeJoke = null;
        private List<JokeVM> _fakeVMJokes = null;
        private JokeVM _fakeVMJoke = null;
        public Guid anyJokeId = Guid.NewGuid();
        public JokesControllerTests()
        {
            Helper.SetupFakeHttpContext();
            _fakeJokes = new List<Joke>() { 
                new Joke() { Id = anyJokeId, Status = JokeStatus.Published, PublishDT = DateTime.Now.AddDays(-1), RateValue = 5 },
                new Joke() { Id = Guid.NewGuid(), Status = JokeStatus.Published, PublishDT = DateTime.Now, RateValue = 4 },
            };
            _fakeJoke = _fakeJokes.ElementAt(0);

            _fakeVMJokes = new List<JokeVM>() {
                new JokeVM() {Id = anyJokeId, PublishDT = DateTime.Now.AddDays(-1), RateValue = 5, Description = "2" },
                new JokeVM() { Id = Guid.NewGuid(), PublishDT = DateTime.Now, RateValue = 4,  Description = "4" },
            };
            _fakeVMJoke = _fakeVMJokes.ElementAt(0);

            _repo = Substitute.For<IJokeRepository>();
            _mapper = Substitute.For<IModelMapper<Joke, JokeVM>>();
            _captcha = Substitute.For<ICaptcha>();
            _jokesController = new JokesController(_repo, _mapper, _captcha);

            _repo.Load(anyJokeId).ReturnsForAnyArgs((x) => _fakeJoke);
            int totalItems;
            int favJokesCount;
            _repo.LoadAll(null, out totalItems).ReturnsForAnyArgs((x) => _fakeJokes);
            _repo.LoadFavourites(null, out favJokesCount).ReturnsForAnyArgs((x) => _fakeJokes);
            _captcha.IsValid().Returns(x => true);

            for (int i = 0; i < _fakeJokes.Count; i++)
            {
                _mapper.MapToViewModel(_fakeJokes.ElementAt(i)).Returns(_fakeVMJokes.ElementAt(i));
            }
            _jokesController.ControllerContext = new ControllerContext(Substitute.For<HttpContextBase>(), new RouteData(), _jokesController);
        }

        [Fact]
        public void Constructor_WhenCallsWithNullParameter_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new JokesController(null, null, null));
            Assert.Throws<ArgumentNullException>(() => new JokesController(null, _mapper, null));
            Assert.Throws<ArgumentNullException>(() => new JokesController(_repo, null, _captcha));
        }

        [Fact]
        public void Constructor_WhenNotAuthenticated_ShouldNotLoadUserRatedJokes()
        {
            AuthUser.Instance.IsAuthenticated = false;
            int totalCount;
            new JokesController(_repo, _mapper, _captcha);
            _repo.DidNotReceiveWithAnyArgs().LoadRated(null, out totalCount);
        }

        [Fact]
        public void Constructor_WhenNotAuthenticated_ShouldNotLoadUserFavouriteJokes()
        {
            AuthUser.Instance.IsAuthenticated = false;
            int totalCount;
            new JokesController(_repo, _mapper, _captcha);
            _repo.DidNotReceiveWithAnyArgs().LoadFavourites(null, out totalCount);
        }

        [Fact]
        public void Constructor_WhenAuthenticated_ShouldLoadUserRatedJokes()
        {
            AuthUser.Instance.IsAuthenticated = true;
            int totalCount;
            new JokesController(_repo, _mapper, _captcha);
            _repo.ReceivedWithAnyArgs().LoadRated(null, out totalCount);
        }

        [Fact]
        public void Constructor_WhenAuthenticated_ShouldLoadUserFavouriteJokes()
        {
            AuthUser.Instance.IsAuthenticated = true;
            int totalCount;
            new JokesController(_repo, _mapper, _captcha);
            _repo.ReceivedWithAnyArgs().LoadFavourites(null, out totalCount);
        }
        [Fact]
        public void Constructor_WhenCallsWithValidParameter_ShouldNotThrowException()
        {
            var ex = Record.Exception(() => new JokesController(_repo, _mapper, _captcha));
            Assert.Null(ex);
        }
        [Fact]
        public void Index_WhenCalls_ShouldMapJokesToViewModel()
        {
            _jokesController.Index();
            _mapper.Received().MapToViewModel(_fakeJoke);
            _mapper.ReceivedWithAnyArgs(_fakeJokes.Count()).MapToViewModel(_fakeJoke);
        }
        [Fact]
        public void Index_WhenCallsWithSortType_ShouldSortBySortType()
        {
            var result = (ViewResult)_jokesController.Index("PublishDT");
            var model = (List<JokeVM>)result.Model;

            Assert.True(model.ElementAt(1).Id == anyJokeId);

            result = (ViewResult)_jokesController.Index("RateValue");
            model = (List<JokeVM>)result.Model;

            Assert.True(model.ElementAt(0).Id == anyJokeId);
        }
        [Fact]
        public void Create_WhenCalls_ShouldMapViewModelToEntityModel()
        {
            _jokesController.Create(_fakeVMJoke);
            _mapper.Received().MapToEntity(_fakeVMJoke);
        }
        [Fact]
        public void Create_WhenCalls_ShouldAdd()
        {
            _mapper.MapToEntity(_fakeVMJoke).Returns(x => _fakeJoke);
            _jokesController.Create(_fakeVMJoke);
            _repo.Received().Add(_fakeJoke);
        }
        [Fact]
        public void Create_WhenTriesToCreateJoke_ShouldValidateCaptcha()
        {
            _jokesController.Create(_fakeVMJoke);
            _captcha.Received().IsValid();
        }
        [Fact]
        public void Create_WhenCaptchaCantValidate_ShouldNotCreateJoke()
        {
            _captcha.IsValid().Returns(x => false);
            _jokesController.Create(new JokeVM());
            _repo.DidNotReceiveWithAnyArgs().Add(null);
        }
        [Fact]
        public void Create_WhenCallsWithInvalidJoke_ShouldNotAdd()
        {
            _jokesController.ModelState.AddModelError("123", "134");
            _jokesController.Create(new JokeVM() {  Description = null });
            _jokesController.Create(new JokeVM() {  Description = null });
            _jokesController.Create(new JokeVM() {  Description = null });
            _repo.DidNotReceiveWithAnyArgs().Add(null);
        }
        [Fact]
        public void Rate_WhenCalls_ShouldRate()
        {
            int rateValue = -1;

            _repo.Rate(anyJokeId, rateValue).Returns(x => new Joke());

            _jokesController.Rate(anyJokeId, rateValue);
            _repo.Received().Rate(anyJokeId, rateValue);
        }
        [Fact]
        public void Rate_WhenCallsWithInvalidParameters_ShoulCorrectParametersBeforedRate()
        {
            int nonValidRate = 200;
            var validRate = 1;
            _repo.Rate(anyJokeId, nonValidRate).ReturnsForAnyArgs(x => new Joke());

            _jokesController.Rate(anyJokeId, nonValidRate);
            _repo.Received().Rate(anyJokeId, validRate);

            _jokesController.Rate(anyJokeId, -20);
            _repo.Received().Rate(anyJokeId, -1);
        }
        [Fact]
        public void Single_WhenCalls_ShouldLoadRelatedJoke()
        {
            _jokesController.Single(anyJokeId);

            _repo.Received().Load(anyJokeId);
        }
        [Fact]
        public void Single_WhenSelectedJokeIsNotPublished_ShouldReturnEmptyModel()
        {
            _repo.Load(anyJokeId).Returns(new Joke() { Status = JokeStatus.PublishRequired });

            var result = (ViewResult)_jokesController.Single(anyJokeId);

            _mapper.DidNotReceiveWithAnyArgs().MapToViewModel(null);
            Assert.True(((IEnumerable<JokeVM>)result.Model).Count() == 0);

        }
        [Fact]
        public void Single_WhenJokeNotExists_ShouldReturnEmptyModel()
        {
            _repo.Load(anyJokeId).Returns(x => null);

            var result = (ViewResult)_jokesController.Single(anyJokeId);

            _mapper.DidNotReceiveWithAnyArgs().MapToViewModel(null);
            Assert.True(((IEnumerable<JokeVM>)result.Model).Count() == 0);
        }
        [Fact]
        public void Single_WhenSelectedJokeIsPublished_ShouldMapToViewModel()
        {
            var joke = new Joke() { Status = JokeStatus.Published };
            _repo.Load(anyJokeId).Returns(joke);

            var result = _jokesController.Single(anyJokeId);

            _mapper.Received().MapToViewModel(joke);
            Assert.True(result.GetType() == typeof(ViewResult));
        }
        [Fact]
        public void DeleteFromFavourites_WhenCalls_ShouldDelete()
        {
            _jokesController.RemoveFromFavourites(anyJokeId);
            _repo.Received().DeleteFromFavourites(anyJokeId, AuthUser.Instance.Id);
        }
        [Fact]
        public void AddToFavourites_WhenCalls_ShouldAdd()
        {
            _jokesController.AddToFavourites(anyJokeId);
            _repo.Received().AddToFavourites(anyJokeId, AuthUser.Instance.Id);
        }
        [Fact]
        public void Favourites_WhenCalls_ShouldMapJokesToViewModel()
        {
            _jokesController.Favourites();
            _mapper.Received().MapToViewModel(_fakeJoke);
            _mapper.ReceivedWithAnyArgs(_fakeJokes.Count()).MapToViewModel(_fakeJoke);
        }
    }
}
