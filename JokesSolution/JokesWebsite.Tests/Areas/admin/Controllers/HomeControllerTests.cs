﻿using JokesLib.Interfaces;
using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using JokesWebsite.Areas.admin.Controllers;
using JokesWebsite.Models.VM;
using JokesWebsite.Tests.Helpers;
using ModelMapping;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Xunit;

namespace JokesWebsite.Tests.Areas.admin.Controllers
{
    public class HomeControllerTests
    {
        IJokeRepository _repo = null;
        IModelMapper<Joke, JokeVM> _mapper = null;
        HomeController _homeController = null;
        private List<Joke> _fakeJokes = null;
        public Guid anyJokeId = Guid.NewGuid();
        public HomeControllerTests()
        {
            Helper.SetupFakeHttpContext();
            _repo = Substitute.For<IJokeRepository>();
            _mapper = Substitute.For<IModelMapper<Joke, JokeVM>>();
            _homeController = new HomeController(_repo, _mapper);
            _homeController.ControllerContext = new ControllerContext(Substitute.For<HttpContextBase>(), new RouteData(), _homeController);

            _fakeJokes = new List<Joke>() { 
                new Joke() { Id = anyJokeId, Status = JokeStatus.Published, PublishDT = DateTime.Now.AddDays(-1), RateValue = 5 },
                new Joke() { Id = Guid.NewGuid(), Status = JokeStatus.Published, PublishDT = DateTime.Now, RateValue = 4 },
            };
            int count;
            _repo.LoadAll(i => i.Status == JokeStatus.PublishRequired, out count).ReturnsForAnyArgs(_fakeJokes);
        }
        [Fact]
        public void Constructor_WhenCallsWithInvalidParameters_ShouldThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => new HomeController(null, null));
            Assert.Throws<ArgumentNullException>(() => new HomeController(null, _mapper));
            Assert.Throws<ArgumentNullException>(() => new HomeController(_repo, null));
        }
        [Fact]
        public void Constructor_WhenCallsWithValidParameters_ShouldNotThrowException()
        {
            Record.Exception(() => new HomeController(_repo, _mapper));
        }
        [Fact]
        public void Index_WhenCalls_ShouldLoadJokes()
        {
            _homeController.Index();
            int totalCount;
            _repo.ReceivedWithAnyArgs().LoadAll(i => i.Status == JokeStatus.PublishRequired, out totalCount);
        }
        [Fact]
        public void Index_WhenCalls_ShouldMapJokesToViewModel()
        {
            _homeController.Index();
            _fakeJokes.ForEach(j => _mapper.ReceivedWithAnyArgs().MapToViewModel(j));
        }
        [Fact]
        public void Index_WhenCalls_ShouldPassJokesToViewAsModel()
        {
            var result = (ViewResult)_homeController.Index();
            Assert.True(((IEnumerable<JokeVM>)result.Model).Count() == _fakeJokes.Count);
        }
        [Fact]
        public void Delete_WhenCalls_ShouldDelete()
        {
            _homeController.Delete(anyJokeId);
            _repo.Received().Delete(anyJokeId);
        }
        [Fact]
        public void Delete_WhenDeletes_ShouldReturnStatusWithJson()
        {
            var result = _homeController.Delete(anyJokeId);
            Assert.True(result.GetType() == typeof(JsonResult));
        }
        [Fact]
        public void Publish_WhenCalls_ShouldPublish()
        {
            _homeController.Publish(anyJokeId);
            _repo.Received().Publish(anyJokeId);
        }
        [Fact]
        public void Publush_whenPublishes_ShouldReturnStatusWithJson()
        {
            var result = _homeController.Delete(anyJokeId);
            Assert.True(result.GetType() == typeof(JsonResult));
        }
    }
}
