﻿using JokesLib.Interfaces.Repositories;
using JokesLib.Models;
using JokesWebsite.Areas.admin.Controllers;
using JokesWebsite.Interfaces;
using JokesWebsite.Tests.Helpers;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Xunit;

namespace JokesWebsite.Tests.Areas.admin.Controllers
{
    public class LoginControllerTests
    {
        private LoginController _loginController = null;
        private IAdminLoginRepository _repo = null;
        private ICaptcha _captcha = null;
        private string user = "beqa";
        private string pass = "123";
        public LoginControllerTests()
        {
            Helper.SetupFakeHttpContext();
            _repo = Substitute.For<IAdminLoginRepository>();
            _captcha = Substitute.For<ICaptcha>();
            _loginController = new LoginController(_repo, _captcha);
            _loginController.ControllerContext = new ControllerContext(Substitute.For<HttpContextBase>(), new RouteData(), _loginController);
            _captcha.IsValid().Returns((x) => true);
        }
        [Fact]
        public void Constructor_WhenPassesRepositoryParameter_ShouldNotThrowException()
        {
            var ex = Record.Exception(() => new LoginController(_repo, _captcha));
            Assert.Null(ex);
        }
        [Fact]
        public void Constructor_WhenPassedRepositoryIsNull_ShouldThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => new LoginController(null, null));
            Assert.Throws<ArgumentNullException>(() => new LoginController(null, _captcha));
            Assert.Throws<ArgumentNullException>(() => new LoginController(_repo, null));
        }
        [Fact]
        public void Index_WhenAdministratorIsAuthenticated_ShouldRedirectToAdminHomePage()
        {
            Administrator.Instance.IsAuthenticated = true;
            var result = (RedirectToRouteResult)_loginController.Index();

            Assert.True(typeof(RedirectToRouteResult) == result.GetType());
            Assert.Equal(((RedirectToRouteResult)result).RouteValues["action"].ToString().ToLower(), "index");
            Assert.Equal(((RedirectToRouteResult)result).RouteValues["controller"].ToString().ToLower(), "home");
        }
        [Fact]
        public void IndexPost_WhenCallsPost_ShouldTryToAuthenticate() 
        {
            _loginController.Index(user, pass);
            _repo.Received().Authenticate(user, JokesLib.Helpers.Helper.EncryptMD5(pass));
        }
        [Fact]
        public void IndexPost_WhenCallsPostWithInvalidParameters_ShouldNotTryToAuthenticate()
        {
            user = "";
            _loginController.Index(user, pass);
            _repo.DidNotReceiveWithAnyArgs().Authenticate(user, JokesLib.Helpers.Helper.EncryptMD5(pass));
        }
        [Fact]
        public void IndexPost_WhenCallsPostWithValidParameters_ShouldValidateCaptcha()
        {
            _loginController.Index(user, pass);
            _captcha.Received().IsValid();
        }
        [Fact]
        public void IndexPost_WhenCallsPostCaptchaFailed_ShouldNotAuthenticate()
        {
            _captcha.IsValid().Returns((x) => false);
            _loginController.Index(user, pass);
            _repo.DidNotReceiveWithAnyArgs().Authenticate(user, pass);
        }
        [Fact]
        public void IndexPost_WhenLoginFailes_ShouldReturnToLoginPage()
        {

            Administrator.Instance.IsAuthenticated = false;
            var result = (ViewResult)_loginController.Index(user, pass);
            Assert.True(result.ViewName == String.Empty);
            Assert.True(typeof(ViewResult) == result.GetType());
        }
        [Fact]
        public void IndexPost_WhenLoginSuccess_ShouldRedirectToAdminHomePage()
        {
            Administrator.Instance.IsAuthenticated = true;
            var result = _loginController.Index(user, pass);
            Assert.True(typeof(RedirectToRouteResult) == result.GetType());
            Assert.Equal(((RedirectToRouteResult)result).RouteValues["action"].ToString().ToLower(), "index");
            Assert.Equal(((RedirectToRouteResult)result).RouteValues["controller"].ToString().ToLower(), "home");
        }
        [Fact]
        public void SignOut_WhenCalls_ShouldSignOut()
        {
            Administrator.Instance.IsAuthenticated = true;
            _loginController.SignOut();
            Assert.True(Administrator.Instance.IsAuthenticated == false);
        }
        [Fact]
        public void SignOut_WhenCalls_ShouldRedirectToLoginPage()
        {
            var result = _loginController.SignOut();
            Assert.True(((RedirectToRouteResult)result).RouteValues["action"].ToString().ToLower() == "index");
        }
    }
}
