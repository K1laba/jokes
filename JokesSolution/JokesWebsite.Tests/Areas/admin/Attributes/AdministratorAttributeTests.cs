﻿using JokesLib.Models;
using JokesWebsite.Areas.admin.Attributes;
using JokesWebsite.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xunit;

namespace JokesWebsite.Tests.Areas.admin.Attributes
{
    public class AdministratorAttributeTests
    {
        private AdministratorAttribute _filter = null;
        private ActionExecutingContext _filterContext = null;

        public AdministratorAttributeTests()
        {
            Helper.SetupFakeHttpContext();

            _filterContext = new ActionExecutingContext();
            _filterContext.HttpContext = new HttpContextWrapper(HttpContext.Current);
            _filter = new AdministratorAttribute();
        }
        [Fact]
        public void OnActionExecuting_WhenAdministratorNotAuthenticated_ShouldRedirectToLoginPage()
        {
            Administrator.Instance.IsAuthenticated = false;

            _filter.OnActionExecuting(_filterContext);

            Assert.NotNull(_filterContext.Result);
            Assert.True(_filterContext.Result.GetType() == typeof(RedirectToRouteResult));
            Assert.True(((RedirectToRouteResult)_filterContext.Result).RouteValues["action"].ToString().ToLower() == "index");
            Assert.True(((RedirectToRouteResult)_filterContext.Result).RouteValues["controller"].ToString().ToLower() == "login");
            Assert.True(((RedirectToRouteResult)_filterContext.Result).RouteValues["area"].ToString().ToLower() == "admin");
        }
    }
}
