﻿using JokesLib.Models;
using JokesWebsite.Attributes;
using JokesWebsite.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xunit;

namespace JokesWebsite.Tests.Attributes
{
    public class AuthorizationRequiredAttributeTests
    {
        private AuthorizationRequiredAttribute _filter = null;
        private ActionExecutingContext _filterContext = null;
        public AuthorizationRequiredAttributeTests()
        {
            Helper.SetupFakeHttpContext();

            _filterContext = new ActionExecutingContext();
            _filterContext.HttpContext = new HttpContextWrapper(HttpContext.Current);
            _filter = new AuthorizationRequiredAttribute();
        }
        [Fact]
        public void OnActionExecuting_WhenNotAuthenitcated_ShouldRedirectToLogin()
        {
            AuthUser.Instance.IsAuthenticated = false;
            
            _filter.OnActionExecuting(_filterContext);

            Assert.NotNull(_filterContext.Result);
            Assert.True(_filterContext.Result.GetType() == typeof(RedirectToRouteResult));
        }
    }
}
